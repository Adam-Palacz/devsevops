resource "azurerm_kubernetes_cluster" "aks-dso-dev" {
  name                = "aks-${var.prefix}"
  location            = azurerm_resource_group.rg-dso-dev.location
  resource_group_name = azurerm_resource_group.rg-dso-dev.name
  dns_prefix          = "dso-ap-dev-dns"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = var.common_tags
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.aks-dso-dev.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.aks-dso-dev.kube_config_raw

  sensitive = true
}