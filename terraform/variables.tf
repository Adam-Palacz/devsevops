variable "region" {
  description = "Cloud region"
  default     = "westeurope"
}

variable "prefix" {
  description = "Resource name prefix"
  default     = "dsodev"
}

variable "common_tags" {
  description = "Common tags to be applied to resources"
  type        = map(string)
  default = {
    "Environment" = "Dev"
    "ManagedBy"   = "Terraform"
  }
}