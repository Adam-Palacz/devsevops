resource "azurerm_resource_group" "rg-dso-dev" {
  name     = "rg-dso-dev"
  location = var.region
  tags     = var.common_tags
}