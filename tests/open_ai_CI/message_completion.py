import openai


def get_completion_from_messages(messages, model="gpt-3.5-turbo", temperature=0.5):
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature,
    )
    return response.choices[0].message["content"]


def collect_messages(agent, target_file, file_name):
    with open(file_name, 'r') as file:
        code = file.read()
        messages = [
            {"role": "system", "content": agent},
            {"role": "user", "content": f"Please review this file: {code}"},
        ]
    response = get_completion_from_messages(messages) 
    with open(f'tests/open_ai_CI/{target_file}.txt', 'w') as new_file:
        new_file.write(file_name + '\n')
        new_file.write("------------------" + '\n')
        new_file.write(response + '\n')