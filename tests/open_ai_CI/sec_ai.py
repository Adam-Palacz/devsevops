import sys
import openai
from message_completion import collect_messages

agent = f"""
You are an Security expert. Your job is to look at the following files and give security recomendations. Return results and code examples in txt format.
"""

target_file = 'sec_review'

if __name__ == "__main__":
    openai.api_key = sys.argv[1]
    file_names = sys.argv[2:]
    for file_name in file_names:
        collect_messages(agent, target_file, file_name)